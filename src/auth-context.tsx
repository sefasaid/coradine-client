import { Color } from "@material-ui/lab";
import React, { createContext, useState, FC, useContext, useEffect } from "react";

export type LocalData = {
    user: {
        username: string | null;
        password: string | null;
    };
    token: string | null;
    alert: {
        type: Color | undefined;
        description: string | null;
    }
}
export type LocalDataState = {
    data: LocalData;
    changeData: (data: {
        user: {
            username: string | null;
            password: string | null;
        };
        token: string | null;
        alert: {
            type: Color | undefined;
            description: string | null;
        };
    }) => void;
};

export const contextDefaultValues: LocalDataState = {
    data: {
        user: {
            username: null,
            password: null
        },
        token: null,
        alert: {
            type: undefined,
            description: null
        }
    },
    changeData: () => { }
};

export const LocalDataContext = createContext<LocalDataState>(contextDefaultValues);

export function useLocalContext() {
    const { data, changeData } = useContext(LocalDataContext);
    const setAlert = (type: Color, description: string) => {
        changeData({ ...data, alert: { type, description } });
        setTimeout(() => {
            changeData({ ...data, alert: { type: undefined, description: null } });
        }, 5000);
    }

    const saveUser = ({ username, password, token }: { username: string, password: string, token: string }) => {
        localStorage.setItem('user', JSON.stringify({ username, password }));
        changeData({ ...data, user: { username, password } })
        localStorage.setItem('token', token);
        changeData({ ...data, token })
    }

    return {
        setAlert,
        saveUser,
    };

}
