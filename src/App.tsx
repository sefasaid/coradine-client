import React, { createContext, useContext, useEffect, useState } from 'react';
import { Alert, AlertTitle, Color } from '@material-ui/lab';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
  useHistory,
  useLocation
} from "react-router-dom";
import SignIn from './auth/Login';
import Dashboard from './pages/Dashboard';
import { contextDefaultValues, LocalData, LocalDataContext, useLocalContext } from './auth-context';
import { AppBar, Box, Container, CssBaseline, Divider, Drawer, IconButton, List, ListItem, ListItemIcon, ListItemText, Toolbar, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import PrintIcon from '@material-ui/icons/Print';
import AddPage from './pages/Add';
const drawerWidth = 240;
import apolloClient from './apollo-service';
import { ApolloProvider } from '@apollo/client';
import EditPage from './pages/Edit';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  menuButtonHidden: {
    display: 'none',
  },
  title: {
    flexGrow: 1,
  },
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9),
    },
  },
  page: {
    display: 'flex',
    flexDirection: 'column',
    flex: 1
  },
  appBarSpacer: theme.mixins.toolbar,
}));

export default function App() {
  const [data, changeData] = useState<LocalData>(contextDefaultValues.data);
  let { setAlert, saveUser } = useLocalContext();
  const classes = useStyles();

  const [open, setOpen] = useState(true);

  const handleDrawerOpen = () => {
    setOpen(true);
  };
  const handleDrawerClose = () => {
    setOpen(false);
  };
  let history = useHistory();

  useEffect(() => {
    const token = localStorage.getItem('token');
    let user = localStorage.getItem('user');
    if (!!token && !!user) {
      const userObject: { username: string, password: string } = JSON.parse(user);
      saveUser({ username: userObject.username, password: userObject.password, token });
    }
  }, []);
  return (

    <LocalDataContext.Provider
      value={{
        data,
        changeData
      }}
    >
      <Router>
        <Switch>
          <Route path="/login" render={({ location }) =>
            data.token ? (
              <Redirect
                to={{
                  pathname: "/app",
                  state: { from: location }
                }}
              />) : (
              <div>
                {!!data.alert.type &&
                  <Alert severity={data.alert.type}>
                    <AlertTitle>Error</AlertTitle>
                    {data.alert.description}
                  </Alert>
                }
                <SignIn />
              </div>
            )
          }
          />
          <Route
            render={({ location }) =>
              data.token ? (
                <div className={classes.root}>
                  <CssBaseline />
                  <AppBar position="absolute" className={clsx(classes.appBar, open && classes.appBarShift)}>
                    <Toolbar className={classes.toolbar}>
                      <IconButton
                        edge="start"
                        color="inherit"
                        aria-label="open drawer"
                        onClick={handleDrawerOpen}
                        className={clsx(classes.menuButton, open && classes.menuButtonHidden)}
                      >
                        <MenuIcon />
                      </IconButton>
                      <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title}>
                        Printers
                      </Typography>
                      <IconButton color="inherit">
                        <ExitToAppIcon />
                      </IconButton>
                    </Toolbar>
                  </AppBar>
                  <Drawer
                    variant="permanent"
                    classes={{
                      paper: clsx(classes.drawerPaper, !open && classes.drawerPaperClose),
                    }}
                    open={open}
                  >
                    <div className={classes.toolbarIcon}>
                      <IconButton onClick={handleDrawerClose}>
                        <ChevronLeftIcon />
                      </IconButton>
                    </div>
                    <Divider />
                    <List>
                      <ListItem button>
                        <ListItemIcon>
                          <PrintIcon />
                        </ListItemIcon>
                        <ListItemText primary="Printers" />
                      </ListItem>
                    </List>
                    <Divider />
                  </Drawer>
                  <ApolloProvider client={apolloClient}>
                    <div className={classes.page}>
                    <div className={classes.appBarSpacer} />

                      {!!data.alert.type &&
                        <Alert severity={data.alert.type}>
                          <AlertTitle>Error</AlertTitle>
                          {data.alert.description}
                        </Alert>
                      }
                      <Switch>
                        <Route exact path="/app">
                          <Dashboard />
                        </Route>
                        <Route exact path="/app/add">
                          <AddPage />
                        </Route>
                        <Route exact path="/app/edit/:id">
                          <EditPage />
                        </Route>
                      </Switch>
                    </div>
                  </ApolloProvider>
                </div>
              ) : (
                <Redirect
                  to={{
                    pathname: "/login",
                    state: { from: location }
                  }}
                />
              )
            }
          />

        </Switch>

      </Router>
    </LocalDataContext.Provider>
  );
}
