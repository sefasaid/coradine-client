import React, { useEffect, useState } from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Drawer from '@material-ui/core/Drawer';
import Box from '@material-ui/core/Box';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Link from '@material-ui/core/Link';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import { Router, Switch, Route, useHistory } from 'react-router';
import { Table, TableHead, TableRow, TableCell, TableBody, ListItem, ListItemIcon, ListItemText, Button, ButtonGroup } from '@material-ui/core';
import PrintIcon from '@material-ui/icons/Print';
import { gql, useMutation } from '@apollo/client';
import apolloClient from '../apollo-service';
import { useLocalContext } from '../auth-context';
function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {'Copyright © '}
            <Link color="inherit" href="https://material-ui.com/">
                Your Website
            </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
    addButton: {
        margin: 10,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'flex-end',
    },
    content: {
        flexGrow: 1,
        height: '100vh',
        overflow: 'auto',
    },
    container: {
        paddingTop: theme.spacing(4),
        paddingBottom: theme.spacing(4),
    },
    paper: {
        padding: theme.spacing(2),
        display: 'flex',
        overflow: 'auto',
        flexDirection: 'column',
    },
    height100: {
        height: '100%',
    },
    appBarSpacer: theme.mixins.toolbar,
}));

const PRINTER_QUERY = gql`
    query Query {
        printers {
            title
            id
            description
        }
    }
`;
const DELETE = gql`
  mutation Mutation($deleteId: ID) {
    delete(id: $deleteId)
  }
`;

export default function Dashboard() {
    const classes = useStyles();
    const { setAlert } = useLocalContext();

    const [printers, setPrinters] = useState<{ id: string, title: string, description: string }[]>([]);
    const [deletePrinterMutate, { loading }] = useMutation(DELETE, {
        onError: (error) => {
            if (!!error?.message) setAlert('error', error?.message);
        },
        onCompleted: async (data) => {
            if (data) {
                await apolloClient.resetStore()
                getPrinters();
            }
        }
    });
    let history = useHistory();

    const height100Paper = clsx(classes.paper, classes.height100);

    const redirectToAdd = () => {
        history.push('/app/add')
    }
    const goToEdit = (id: string) => {
        history.push('/app/edit/' + id);
    }
    const deletePrinter = async (id: string) => {
        await deletePrinterMutate({ variables: { deleteId: id } });
    }
    const getPrinters = async () => {
        try {
            const response = await apolloClient
                .query({
                    query: PRINTER_QUERY
                });
            setPrinters(response.data.printers);
        } catch (error) {
        }
    }
    useEffect(() => {
        getPrinters();
    }, [])
    return (
        <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <div className={classes.addButton}>
                <ButtonGroup color="primary" aria-label="outlined primary button group">
                    <Button onClick={redirectToAdd}>Add</Button>
                </ButtonGroup>
            </div>
            <Container maxWidth="lg" className={classes.container}>
                <Grid container spacing={3}>
                    {/* List */}
                    <Grid item xs={12} md={12} lg={12}>
                        <Paper className={height100Paper}>
                            <Table size="small">
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Id</TableCell>
                                        <TableCell>Title</TableCell>
                                        <TableCell>Description</TableCell>
                                        <TableCell align="right">Settings</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {printers.map((printer) => (
                                        <TableRow key={printer.id}>
                                            <TableCell>{printer.id}</TableCell>
                                            <TableCell>{printer.title}</TableCell>
                                            <TableCell>{printer.description}</TableCell>
                                            <TableCell align="right">
                                                <ButtonGroup color="primary" aria-label="outlined primary button group">
                                                    <Button onClick={() => { goToEdit(printer.id) }}>Edit</Button>
                                                    <Button onClick={() => { deletePrinter(printer.id) }}>Delete</Button>
                                                </ButtonGroup>
                                            </TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>

                        </Paper>
                    </Grid>
                </Grid>

                <Box pt={4}>
                    <Copyright />
                </Box>
            </Container>
        </main>

    );
}