import React, { useEffect, useState } from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Link from '@material-ui/core/Link';
import { Router, Switch, Route, useHistory } from 'react-router';
import { Button, TextField } from '@material-ui/core';
import { gql, useMutation } from '@apollo/client';
import { useLocalContext } from '../auth-context';
import apolloClient from '../apollo-service';
function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {'Copyright © '}
            <Link color="inherit" href="https://material-ui.com/">
                Your Website
            </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}


const useStyles = makeStyles((theme) => ({
    addButton: {
        margin: 10,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'flex-end',
    },
    content: {
        flexGrow: 1,
        height: '100vh',
        overflow: 'auto',
    },
    container: {
        paddingTop: theme.spacing(4),
        paddingBottom: theme.spacing(4),
        alignItems: 'center',

    },
    paper: {
        padding: theme.spacing(2),
        display: 'flex',
        overflow: 'auto',
        flexDirection: 'column',
    },

    appBarSpacer: theme.mixins.toolbar,
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    center: {
        alignSelf: 'center',
        flexDirection: 'column'
    }
}));
const ADD = gql`
  # Increments a back-end counter and gets its resulting value
  mutation Mutation($createPrinter: Input) {
    create(printer: $createPrinter) {
        id
    }
  }
`;

export default function AddPage() {
    const classes = useStyles();
    let history = useHistory();
    const { setAlert } = useLocalContext();
    const [formData, setFormData] = useState({ title: "", description: "" });
    const [addPrinter, { data, loading }] = useMutation(ADD, {
        onError: (error) => {
            if (!!error?.message) setAlert('error', error?.message);
        }
    });

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setFormData({ ...formData, [e.target.name]: e.target.value })
    }
    const height100Paper = clsx(classes.paper);


    const add = async () => {
        await addPrinter({ variables: { createPrinter: formData } });
    }
    useEffect(() => {
        if (!!data) {
            apolloClient.resetStore();
            history.goBack();
        }
    }, [data])
    useEffect(() => {
    }, [loading])

    return (
        <main className={classes.content}>
            <div className={classes.appBarSpacer} />

            <Container maxWidth="lg" className={classes.container}>
                <Grid container spacing={3} className={classes.center}>
                    <Paper className={height100Paper}>
                        <form className={classes.form} noValidate >
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                id="title"
                                label="Title"
                                name="title"
                                autoFocus
                                onChange={handleChange}
                            />
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                name="description"
                                label="Description"
                                id="description"
                                onChange={handleChange}
                            />
                            <Button
                                type="button"
                                fullWidth
                                variant="contained"
                                color="primary"
                                className={classes.submit}
                                onClick={add}
                            >
                                Add
                            </Button>
                        </form>
                    </Paper>
                </Grid>
                <Box pt={4}>
                    <Copyright />
                </Box>
            </Container>
        </main>
    );
}