import React, { useEffect, useState } from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Link from '@material-ui/core/Link';
import { Router, Switch, Route, useHistory, useParams } from 'react-router';
import { Button, TextField } from '@material-ui/core';
import { gql, useMutation } from '@apollo/client';
import { useLocalContext } from '../auth-context';
import apolloClient from '../apollo-service';
function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {'Copyright © '}
            <Link color="inherit" href="https://material-ui.com/">
                Your Website
            </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}


const useStyles = makeStyles((theme) => ({
    addButton: {
        margin: 10,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'flex-end',
    },
    content: {
        flexGrow: 1,
        height: '100vh',
        overflow: 'auto',
    },
    container: {
        paddingTop: theme.spacing(4),
        paddingBottom: theme.spacing(4),
        alignItems: 'center',

    },
    paper: {
        padding: theme.spacing(2),
        display: 'flex',
        overflow: 'auto',
        flexDirection: 'column',
    },

    appBarSpacer: theme.mixins.toolbar,
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    center: {
        alignSelf: 'center',
        flexDirection: 'column'
    }
}));
const PRINTER_QUERY = gql`
    query Query($printerId: ID) {
        printer(id: $printerId) {
            title
            description
        }
    }
`;
const EDIT = gql`
    mutation Mutation($editId: ID, $editPrinter: Input) {
        edit(id: $editId, printer: $editPrinter) {
          id
          title
          description
        }
    }
`;

export default function EditPage() {
    let { id } = useParams<any>();
    const classes = useStyles();
    let history = useHistory();
    const { setAlert } = useLocalContext();
    const [printer, setPrinter] = useState({ title: "", description: "" });
    const [editPrinter, { loading }] = useMutation(EDIT, {
        onError: (error) => {
            if (!!error?.message) setAlert('error', error?.message);
        },
        onCompleted: async (data) => {
            if (!!data) {
                await apolloClient.resetStore();
                history.goBack();
            }
        }
    });

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setPrinter({ ...printer, [e.target.name]: e.target.value })
    }
    const height100Paper = clsx(classes.paper);


    const edit = async () => {
        await editPrinter({ variables: { editId: id, editPrinter: printer } });
    }
    const getPrinter = async () => {
        try {
            const response = await apolloClient
                .query({
                    query: PRINTER_QUERY,
                    variables: { printerId: id }
                });
            setPrinter({ title: response.data.printer.title, description: response.data.printer.description });
        } catch (error) {
        }
    }

    useEffect(() => {
        getPrinter()
    }, [id])

    return (
        <main className={classes.content}>
            <div className={classes.appBarSpacer} />

            <Container maxWidth="lg" className={classes.container}>
                <Grid container spacing={3} className={classes.center}>
                    <Paper className={height100Paper}>
                        <form className={classes.form} noValidate >
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                value={printer.title}
                                fullWidth
                                id="title"
                                label="Title"
                                name="title"
                                autoFocus
                                onChange={handleChange}
                            />
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                value={printer.description}
                                fullWidth
                                name="description"
                                label="Description"
                                id="description"
                                onChange={handleChange}
                            />
                            <Button
                                type="button"
                                fullWidth
                                variant="contained"
                                color="primary"
                                className={classes.submit}
                                onClick={edit}
                            >
                                Edit
                            </Button>
                        </form>
                    </Paper>
                </Grid>
                <Box pt={4}>
                    <Copyright />
                </Box>
            </Container>
        </main>
    );
}